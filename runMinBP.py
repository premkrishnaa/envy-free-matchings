import cplex
from genLP import *

codes = [1, 101]

folders = ['n1_1000_n2_100_k_15', 'n1_1000_n2_100_k_5',
           'n1_1000_n2_10_k_5', 'n1_1000_n2_20_k_5']
paths = ['shuffle', 'master']
f = open('minBP_LP_stats.txt', 'w', buffering=0)
for path in paths:
    f.write(path + '\n')
    for folder in folders:
        f.write(folder + '\n')
        path_split = folder.split('_')
        n1 = int(path_split[1])
        n2 = int(path_split[3])
        k = int(path_split[5])
        cap = int(n1/n2)
        fileprefix = str(n1) + '_' + str(n2) + '_' + str(k) + '_' + str(cap) + '_'
        avg_bp = 0.0
        avg_size = 0.0
        for i in range(1,11):
            f.write('\t#{}:\t'.format(i))
            fullpath = 'LP_HRLQ' + '/' + path + '/' + folder + '/' + fileprefix + str(i) + '_.txt'
            model = cplex.Cplex(fullpath)
            model.solve()
            status = model.solution.get_status()
            f.write("Solution status: {},\t".format(status))
            if(status in codes):
                soln = model.solution.get_objective_value()
                avg_bp += (soln * 0.1)
                var = model.variables.get_names()
                add = 0.0
                for v in var:
                    add += model.solution.get_values(v)
                add -= soln
                f.write("Blocking pairs: {},\t".format(soln))
                f.write("Matching size: {}\n".format(add))
                avg_size += (add * 0.1)

        f.write("\tAverage BPs: {},\tAverage size: {}\n\n".format(avg_bp, avg_size))

f.close()