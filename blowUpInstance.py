import sys
sys.path.insert(0, 'classes/')
from graph import *

def getOnetoOneInstance(g):
    g_one = Graph()

    for r in g.residents:
        g_one.residents.append(Resident(r.name))

    for h in g.hospitals:
        for i in range(1, h.hq+1):
            h_one = None
            if(i < h.lq+1):
                h_one = Hospital(h.name + '#' + str(i), 1, 1)
            else:
                h_one = Hospital(h.name + '#' + str(i), 0, 1)

            for r in h.pref:
                h_one.pref.append(g_one.getResident(r.name))
                g_one.edges.append(Edge(r.name[1:], h_one.name[1:]))

            g_one.hospitals.append(h_one)

    for r in g.residents:
        r_one = g_one.getResident(r.name)
        for h in r.pref:
            for i in range(1, h.hq+1):
                r_one.pref.append(g_one.getHospital(h.name + '#' + str(i)))

    return g_one