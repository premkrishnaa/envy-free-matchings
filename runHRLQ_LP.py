import cplex
from genLP import *

codes = [1, 101]

folders = ['n1_1000_n2_1000_k_5', 'n1_1000_n2_1000_k_10', 'n1_1000_n2_1000_k_20', 'n1_500_n2_500_k_5',
           'n1_500_n2_500_k_10', 'n1_500_n2_500_k_20', 'n1_100_n2_100_k_5', 'n1_100_n2_100_k_10',
           'n1_100_n2_100_k_20', 'n1_50_n2_50_k_5']

folders = ['n1_100_n2_100_k_20']

paths = ['shuffle', 'master']

f = open('hrlq_stats_LP_repeat.txt', 'w', buffering=0)
for path in paths:
    f.write(path + '\n')
    for folder in folders:
        f.write(folder + '\n')
        path_split = folder.split('_')
        n1 = int(path_split[1])
        n2 = int(path_split[3])
        k = int(path_split[5])
        cap = int(n1/n2)
        fileprefix = str(n1) + '_' + str(n2) + '_' + str(k) + '_' + str(cap) + '_'
        avg = 0.0
        ct = 0.0
        for i in range(1,11):
            f.write('\t#{}:\t'.format(i))
            fullpath = 'LP_HRLQ/HRLQ_0_1/tuples/' + path + '/' + folder + '/' + fileprefix + str(i) + '.txt'
            fullpath_lq = 'LP_HRLQ/HRLQ_0_1/tuples/' + path + '/' + folder + '/' + fileprefix + str(i) + '_lq.txt'
            model = cplex.Cplex(fullpath)
            model.solve()
            status = model.solution.get_status()
            flq = open(fullpath_lq, 'r')
            lq_hosps = flq.readlines()[0].split(',')[:-1]
            for j, h in enumerate(lq_hosps):
                lq_hosps[j] = h[1:]
            f.write("Solution status: {},\t".format(status))
            f_soln = open('vars_' + str(i) + '_int.txt', 'w', buffering=0)
            if(status in codes):
                soln = model.solution.get_objective_value()
                avg += soln
                ct += 1
                var = model.variables.get_names()
                for v in var:
                    v_h = v.split('_')[2]
                    f_soln.write(v + ',' + str(model.solution.get_values(v)) + '\n')
                f.write("Size: {}\n".format(soln))
            else:
                f.write("Size: infeasible\n")

        if(ct != 0):
            f.write("\tAverage Size: {}\n\n".format(avg/ct))
        else:
            f.write("\tAverage Size: not defined\n\n")

        flq.close()

f.close()