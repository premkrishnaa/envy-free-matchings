def checkneq(n):
    return (abs(1 - n) > 10e-5)

def claim1(id):
    f = open('vars_' + id + '.txt')
    f_int = open('vars_' + id + '_int.txt')

    res = {}
    res_int = {}

    for line in f.readlines():
        l_split = line.split(',')
        var = l_split[0]
        val = float(l_split[1])
        r = var.split('_')[1]
        if(r not in res):
            res[r] = val
        else:
            res[r] = res[r] + val

    for line in f_int.readlines():
        l_split = line.split(',')
        var = l_split[0]
        val = float(l_split[1])
        r = var.split('_')[1]
        if(r not in res_int):
            res_int[r] = val
        else:
            res_int[r] = res_int[r] + val

    for r in res_int.keys():
        if(float(res_int[r]) == float(1.0)):
            if(checkneq(float(res[r]))):
                print('claim false! r' + r + ' ' + str(res_int[r]) + ',' + str(res[r]))

    f.close()
    f_int.close()

def claim2(id):
    f = open('vars_' + id + '.txt')
    f_int = open('vars_' + id + '_int.txt')
    flq = open('LP_HRLQ/HRLQ_0_1/tuples/master/n1_100_n2_100_k_20/100_100_20_1_' + id + '_lq.txt')

    res = {}
    res_int = {}
    counts = {}

    lq_hosps = flq.readlines()[0].split(',')[:-1]
    for j, h in enumerate(lq_hosps):
        lq_hosps[j] = h[1:]

    print(lq_hosps)

    for line in f.readlines():
        l_split = line.split(',')
        var = l_split[0]
        val = float(l_split[1])
        varsplit = var.split('_')
        r = varsplit[1]
        h = varsplit[2]
        if(r not in counts):
            counts[r] = 0
        if(r not in res):
            res[r] = val
        else:
            res[r] = res[r] + val

        if(h in lq_hosps and val > 0):
            counts[r] = counts[r] + 1

    for line in f_int.readlines():
        l_split = line.split(',')
        var = l_split[0]
        val = float(l_split[1])
        r = var.split('_')[1]
        if(r not in res_int):
            res_int[r] = val
        else:
            res_int[r] = res_int[r] + val

    for r in res_int.keys():
        if(counts[r] > 0 and (not checkneq(res[r]))):
            if(float(res_int[r]) != float(1)):
                print('claim false! r' + r + ' ' + str(res_int[r]) + ',' + str(res[r]) + ',' + str(counts[r]))

    f.close()
    f_int.close()
    flq.close()

def main():
    for i in ['7', '9', '10']:
        print('******* ' + i + ' *******')
        claim1(i)
        print('\n\n')

    for i in ['7', '9', '10']:
        print('******* ' + i + ' *******')
        claim2(i)
        print('\n\n')

if __name__ == '__main__':
    main()