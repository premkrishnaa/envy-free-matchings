import os
import cplex
from stableMatching import *
from genLP import *
from yokoiInstance import *

folders = ['n1_1000_n2_1000_k_5', 'n1_1000_n2_1000_k_10', 'n1_1000_n2_1000_k_20', 'n1_500_n2_500_k_5',
           'n1_500_n2_500_k_10', 'n1_500_n2_500_k_20', 'n1_100_n2_100_k_5', 'n1_100_n2_100_k_10',
           'n1_100_n2_100_k_20', 'n1_50_n2_50_k_5']

paths = ['shuffle', 'master']
f = open('hrlq_stats.txt', 'w', buffering=0)
for path in paths:
    f.write(path + '\n')
    for folder in folders:
        f.write(folder + '\n')
        path_split = folder.split('_')
        n1 = int(path_split[1])
        n2 = int(path_split[3])
        k = int(path_split[5])
        cap = int(n1/n2)
        fileprefix = str(n1) + '_' + str(n2) + '_' + str(k) + '_' + str(cap) + '_'
        for i in range(1,11):
            fullpath = 'base_instances' + '/' + path + '/' + folder + '/' + fileprefix + str(i) + '.txt'
            g = createGraph(fullpath, 2)
            m = getStableMatching(g)
            generateMaxCardLP(g, 'temp.txt')
            model = cplex.Cplex('temp.txt')
            model.solve()
            var = model.variables.get_names()
            ct = 0
            lq_hosps = ''
            for v in var:
                v_split = v.split('_')
                h_name = 'h' + v_split[2]
                if(model.solution.get_values(v) != 0.0):
                    h = g.getHospital(h_name)
                    if(len(h.pref) > 0 and len(h.matched) == 0):
                        h.lq = 1
                        lq_hosps += h_name + ','
                        ct += 1

            g_y = getYokoiInstance(g)
            m_y = getStableMatching(g_y)
            f.write('#' + str(i) + ': ' + str(ct) + ', stable: ' + str(len(m)) + 
                    ', max: ' + str(model.solution.get_objective_value()) + 
                    ', envy-feasible: ' + str(checkEnvyFreeFeasibility(g_y)) + '\n')
            writepath1 = 'LP_HRLQ/HRLQ_0_1/tuples/' + path + '/' + folder + '/' + fileprefix + str(i) + '.txt'
            writepath2 = 'LP_HRLQ/HRLQ_0_1/tuples/' + path + '/' + folder + '/' + fileprefix + str(i) + '_.txt'
            writepath3 = 'LP_HRLQ/HRLQ_0_1/tuples/' + path + '/' + folder + '/' + fileprefix + str(i) + '_lq.txt'
            generateEnvyFreeLP(g, writepath1, 1)
            generateEnvyFreeLP(g, writepath2, 0)
            flq = open(writepath3, 'w')
            flq.write(lq_hosps)

os.remove('temp.txt')
f.close()