shuffle
n1_1000_n2_1000_k_5
	#1:	Solution status: 103,	Size: infeasible
	#2:	Solution status: 103,	Size: infeasible
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 101,	Size: 783.0
	#7:	Solution status: 101,	Size: 775.0
	#8:	Solution status: 101,	Size: 795.0
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 784.333333333

n1_1000_n2_1000_k_10
	#1:	Solution status: 103,	Size: infeasible
	#2:	Solution status: 103,	Size: infeasible
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 101,	Size: 847.0
	#7:	Solution status: 103,	Size: infeasible
	#8:	Solution status: 101,	Size: 846.0
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 101,	Size: 843.0
	Average Size: 845.333333333

n1_1000_n2_1000_k_20
	#1:	Solution status: 101,	Size: 905.0
	#2:	Solution status: 101,	Size: 920.0
	#3:	Solution status: 101,	Size: 907.0
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 101,	Size: 913.0
	#6:	Solution status: 103,	Size: infeasible
	#7:	Solution status: 103,	Size: infeasible
	#8:	Solution status: 101,	Size: 915.0
	#9:	Solution status: 101,	Size: 908.0
	#10:	Solution status: 101,	Size: 908.0
	Average Size: 910.857142857

n1_500_n2_500_k_5
	#1:	Solution status: 103,	Size: infeasible
	#2:	Solution status: 101,	Size: 384.0
	#3:	Solution status: 101,	Size: 389.0
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 101,	Size: 378.0
	#6:	Solution status: 103,	Size: infeasible
	#7:	Solution status: 101,	Size: 375.0
	#8:	Solution status: 101,	Size: 392.0
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 383.6

n1_500_n2_500_k_10
	#1:	Solution status: 103,	Size: infeasible
	#2:	Solution status: 101,	Size: 429.0
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 101,	Size: 423.0
	#5:	Solution status: 101,	Size: 428.0
	#6:	Solution status: 101,	Size: 422.0
	#7:	Solution status: 101,	Size: 415.0
	#8:	Solution status: 101,	Size: 435.0
	#9:	Solution status: 101,	Size: 434.0
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 426.571428571

n1_500_n2_500_k_20
	#1:	Solution status: 101,	Size: 454.0
	#2:	Solution status: 101,	Size: 463.0
	#3:	Solution status: 101,	Size: 466.0
	#4:	Solution status: 101,	Size: 453.0
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 101,	Size: 454.0
	#7:	Solution status: 103,	Size: infeasible
	#8:	Solution status: 103,	Size: infeasible
	#9:	Solution status: 101,	Size: 452.0
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 457.0

n1_100_n2_100_k_5
	#1:	Solution status: 101,	Size: 83.0
	#2:	Solution status: 101,	Size: 78.0
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 101,	Size: 77.0
	#5:	Solution status: 101,	Size: 81.0
	#6:	Solution status: 101,	Size: 77.0
	#7:	Solution status: 101,	Size: 78.0
	#8:	Solution status: 101,	Size: 79.0
	#9:	Solution status: 101,	Size: 79.0
	#10:	Solution status: 101,	Size: 81.0
	Average Size: 79.2222222222

n1_100_n2_100_k_10
	#1:	Solution status: 101,	Size: 85.0
	#2:	Solution status: 101,	Size: 86.0
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 101,	Size: 85.0
	#5:	Solution status: 101,	Size: 85.0
	#6:	Solution status: 101,	Size: 88.0
	#7:	Solution status: 101,	Size: 91.0
	#8:	Solution status: 101,	Size: 87.0
	#9:	Solution status: 101,	Size: 84.0
	#10:	Solution status: 101,	Size: 90.0
	Average Size: 86.7777777778

n1_100_n2_100_k_20
	#1:	Solution status: 101,	Size: 95.0
	#2:	Solution status: 101,	Size: 97.0
	#3:	Solution status: 101,	Size: 95.0
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 101,	Size: 96.0
	#6:	Solution status: 101,	Size: 93.0
	#7:	Solution status: 103,	Size: infeasible
	#8:	Solution status: 101,	Size: 93.0
	#9:	Solution status: 101,	Size: 90.0
	#10:	Solution status: 101,	Size: 95.0
	Average Size: 94.25

n1_50_n2_50_k_5
	#1:	Solution status: 101,	Size: 39.0
	#2:	Solution status: 101,	Size: 41.0
	#3:	Solution status: 101,	Size: 37.0
	#4:	Solution status: 101,	Size: 38.0
	#5:	Solution status: 101,	Size: 37.0
	#6:	Solution status: 101,	Size: 42.0
	#7:	Solution status: 101,	Size: 41.0
	#8:	Solution status: 101,	Size: 41.0
	#9:	Solution status: 101,	Size: 43.0
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 39.8888888889

master
n1_1000_n2_1000_k_5
	#1:	Solution status: 103,	Size: infeasible
	#2:	Solution status: 103,	Size: infeasible
	#3:	Solution status: 101,	Size: 730.0
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 103,	Size: infeasible
	#7:	Solution status: 101,	Size: 740.0
	#8:	Solution status: 103,	Size: infeasible
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 735.0

n1_1000_n2_1000_k_10
	#1:	Solution status: 103,	Size: infeasible
	#2:	Solution status: 101,	Size: 788.0
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 103,	Size: infeasible
	#7:	Solution status: 101,	Size: 805.0
	#8:	Solution status: 103,	Size: infeasible
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 796.5

n1_1000_n2_1000_k_20
	#1:	Solution status: 101,	Size: 813.0
	#2:	Solution status: 101,	Size: 842.0
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 103,	Size: infeasible
	#7:	Solution status: 103,	Size: infeasible
	#8:	Solution status: 103,	Size: infeasible
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 101,	Size: 844.0
	Average Size: 833.0

n1_500_n2_500_k_5
	#1:	Solution status: 101,	Size: 367.0
	#2:	Solution status: 103,	Size: infeasible
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 101,	Size: 366.0
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 101,	Size: 364.0
	#7:	Solution status: 103,	Size: infeasible
	#8:	Solution status: 103,	Size: infeasible
	#9:	Solution status: 101,	Size: 350.0
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 361.75

n1_500_n2_500_k_10
	#1:	Solution status: 103,	Size: infeasible
	#2:	Solution status: 103,	Size: infeasible
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 101,	Size: 379.0
	#6:	Solution status: 101,	Size: 406.0
	#7:	Solution status: 103,	Size: infeasible
	#8:	Solution status: 103,	Size: infeasible
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 103,	Size: infeasible
	Average Size: 392.5

n1_500_n2_500_k_20
	#1:	Solution status: 101,	Size: 423.0
	#2:	Solution status: 101,	Size: 399.0
	#3:	Solution status: 103,	Size: infeasible
	#4:	Solution status: 101,	Size: 421.0
	#5:	Solution status: 101,	Size: 396.0
	#6:	Solution status: 103,	Size: infeasible
	#7:	Solution status: 101,	Size: 416.0
	#8:	Solution status: 101,	Size: 404.0
	#9:	Solution status: 103,	Size: infeasible
	#10:	Solution status: 101,	Size: 391.0
	Average Size: 407.142857143

n1_100_n2_100_k_5
	#1:	Solution status: 101,	Size: 68.0
	#2:	Solution status: 103,	Size: infeasible
	#3:	Solution status: 101,	Size: 73.0
	#4:	Solution status: 101,	Size: 76.0
	#5:	Solution status: 101,	Size: 75.0
	#6:	Solution status: 101,	Size: 77.0
	#7:	Solution status: 101,	Size: 78.0
	#8:	Solution status: 101,	Size: 70.0
	#9:	Solution status: 101,	Size: 73.0
	#10:	Solution status: 101,	Size: 75.0
	Average Size: 73.8888888889

n1_100_n2_100_k_10
	#1:	Solution status: 101,	Size: 78.0
	#2:	Solution status: 101,	Size: 81.0
	#3:	Solution status: 101,	Size: 78.0
	#4:	Solution status: 103,	Size: infeasible
	#5:	Solution status: 101,	Size: 78.0
	#6:	Solution status: 101,	Size: 75.0
	#7:	Solution status: 101,	Size: 82.0
	#8:	Solution status: 101,	Size: 73.0
	#9:	Solution status: 101,	Size: 81.0
	#10:	Solution status: 101,	Size: 72.0
	Average Size: 77.5555555556

n1_100_n2_100_k_20
	#1:	Solution status: 101,	Size: 81.0
	#2:	Solution status: 101,	Size: 76.0
	#3:	Solution status: 101,	Size: 90.0
	#4:	Solution status: 101,	Size: 88.0
	#5:	Solution status: 101,	Size: 80.0
	#6:	Solution status: 101,	Size: 80.0
	#7:	Solution status: 101,	Size: 79.0
	#8:	Solution status: 101,	Size: 76.0
	#9:	Solution status: 101,	Size: 83.0
	#10:	Solution status: 101,	Size: 84.0
	Average Size: 81.7

n1_50_n2_50_k_5
	#1:	Solution status: 101,	Size: 36.0
	#2:	Solution status: 101,	Size: 35.0
	#3:	Solution status: 101,	Size: 37.0
	#4:	Solution status: 101,	Size: 36.0
	#5:	Solution status: 103,	Size: infeasible
	#6:	Solution status: 101,	Size: 38.0
	#7:	Solution status: 101,	Size: 35.0
	#8:	Solution status: 101,	Size: 39.0
	#9:	Solution status: 101,	Size: 39.0
	#10:	Solution status: 101,	Size: 37.0
	Average Size: 36.8888888889

