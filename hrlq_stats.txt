shuffle
n1_1000_n2_1000_k_5
#1: 89, stable: 782, max: 870.0, envy-feasible: False
#2: 74, stable: 762, max: 834.0, envy-feasible: False
#3: 73, stable: 787, max: 860.0, envy-feasible: False
#4: 86, stable: 800, max: 883.0, envy-feasible: False
#5: 71, stable: 791, max: 861.0, envy-feasible: False
#6: 66, stable: 792, max: 856.0, envy-feasible: True
#7: 81, stable: 782, max: 860.0, envy-feasible: True
#8: 74, stable: 804, max: 877.0, envy-feasible: True
#9: 66, stable: 777, max: 842.0, envy-feasible: False
#10: 75, stable: 761, max: 833.0, envy-feasible: False
n1_1000_n2_1000_k_10
#1: 76, stable: 858, max: 934.0, envy-feasible: False
#2: 89, stable: 851, max: 939.0, envy-feasible: False
#3: 71, stable: 877, max: 946.0, envy-feasible: False
#4: 80, stable: 876, max: 956.0, envy-feasible: False
#5: 82, stable: 857, max: 939.0, envy-feasible: False
#6: 85, stable: 856, max: 940.0, envy-feasible: True
#7: 70, stable: 865, max: 935.0, envy-feasible: False
#8: 79, stable: 860, max: 938.0, envy-feasible: True
#9: 83, stable: 867, max: 950.0, envy-feasible: False
#10: 88, stable: 865, max: 953.0, envy-feasible: True
n1_1000_n2_1000_k_20
#1: 62, stable: 919, max: 981.0, envy-feasible: True
#2: 58, stable: 927, max: 985.0, envy-feasible: True
#3: 62, stable: 923, max: 985.0, envy-feasible: True
#4: 65, stable: 921, max: 986.0, envy-feasible: False
#5: 58, stable: 927, max: 985.0, envy-feasible: True
#6: 56, stable: 929, max: 985.0, envy-feasible: False
#7: 53, stable: 929, max: 982.0, envy-feasible: False
#8: 59, stable: 933, max: 992.0, envy-feasible: True
#9: 62, stable: 924, max: 986.0, envy-feasible: True
#10: 64, stable: 922, max: 986.0, envy-feasible: True
n1_500_n2_500_k_5
#1: 42, stable: 387, max: 429.0, envy-feasible: False
#2: 39, stable: 393, max: 432.0, envy-feasible: True
#3: 43, stable: 391, max: 434.0, envy-feasible: True
#4: 29, stable: 390, max: 419.0, envy-feasible: False
#5: 31, stable: 384, max: 415.0, envy-feasible: True
#6: 43, stable: 387, max: 429.0, envy-feasible: False
#7: 44, stable: 380, max: 423.0, envy-feasible: True
#8: 38, stable: 395, max: 432.0, envy-feasible: True
#9: 46, stable: 393, max: 438.0, envy-feasible: False
#10: 32, stable: 406, max: 437.0, envy-feasible: False
n1_500_n2_500_k_10
#1: 47, stable: 429, max: 476.0, envy-feasible: False
#2: 38, stable: 433, max: 471.0, envy-feasible: True
#3: 49, stable: 423, max: 472.0, envy-feasible: False
#4: 40, stable: 434, max: 474.0, envy-feasible: True
#5: 33, stable: 442, max: 475.0, envy-feasible: True
#6: 45, stable: 431, max: 476.0, envy-feasible: True
#7: 42, stable: 425, max: 467.0, envy-feasible: True
#8: 33, stable: 440, max: 473.0, envy-feasible: True
#9: 33, stable: 438, max: 471.0, envy-feasible: True
#10: 45, stable: 436, max: 481.0, envy-feasible: False
n1_500_n2_500_k_20
#1: 34, stable: 459, max: 493.0, envy-feasible: True
#2: 27, stable: 467, max: 494.0, envy-feasible: True
#3: 24, stable: 472, max: 496.0, envy-feasible: True
#4: 32, stable: 461, max: 493.0, envy-feasible: True
#5: 34, stable: 460, max: 494.0, envy-feasible: False
#6: 30, stable: 462, max: 492.0, envy-feasible: True
#7: 35, stable: 461, max: 496.0, envy-feasible: False
#8: 27, stable: 464, max: 491.0, envy-feasible: False
#9: 35, stable: 461, max: 496.0, envy-feasible: True
#10: 26, stable: 464, max: 490.0, envy-feasible: False
n1_100_n2_100_k_5
#1: 7, stable: 84, max: 90.0, envy-feasible: True
#2: 7, stable: 78, max: 85.0, envy-feasible: True
#3: 11, stable: 73, max: 84.0, envy-feasible: False
#4: 7, stable: 79, max: 86.0, envy-feasible: True
#5: 6, stable: 82, max: 88.0, envy-feasible: True
#6: 7, stable: 77, max: 84.0, envy-feasible: True
#7: 7, stable: 78, max: 85.0, envy-feasible: True
#8: 8, stable: 79, max: 87.0, envy-feasible: True
#9: 7, stable: 80, max: 87.0, envy-feasible: True
#10: 5, stable: 81, max: 86.0, envy-feasible: True
n1_100_n2_100_k_10
#1: 9, stable: 86, max: 95.0, envy-feasible: True
#2: 5, stable: 86, max: 91.0, envy-feasible: True
#3: 9, stable: 87, max: 96.0, envy-feasible: False
#4: 8, stable: 87, max: 95.0, envy-feasible: True
#5: 6, stable: 88, max: 94.0, envy-feasible: True
#6: 8, stable: 89, max: 97.0, envy-feasible: True
#7: 6, stable: 91, max: 97.0, envy-feasible: True
#8: 5, stable: 87, max: 92.0, envy-feasible: True
#9: 5, stable: 88, max: 93.0, envy-feasible: True
#10: 6, stable: 90, max: 96.0, envy-feasible: True
n1_100_n2_100_k_20
#1: 4, stable: 96, max: 100.0, envy-feasible: True
#2: 3, stable: 97, max: 100.0, envy-feasible: True
#3: 5, stable: 95, max: 100.0, envy-feasible: True
#4: 5, stable: 95, max: 100.0, envy-feasible: False
#5: 3, stable: 97, max: 100.0, envy-feasible: True
#6: 7, stable: 93, max: 100.0, envy-feasible: True
#7: 7, stable: 93, max: 100.0, envy-feasible: False
#8: 4, stable: 95, max: 99.0, envy-feasible: True
#9: 8, stable: 90, max: 98.0, envy-feasible: True
#10: 5, stable: 95, max: 100.0, envy-feasible: True
n1_50_n2_50_k_5
#1: 3, stable: 39, max: 42.0, envy-feasible: True
#2: 2, stable: 41, max: 43.0, envy-feasible: True
#3: 4, stable: 37, max: 41.0, envy-feasible: True
#4: 2, stable: 40, max: 42.0, envy-feasible: True
#5: 5, stable: 38, max: 43.0, envy-feasible: True
#6: 5, stable: 42, max: 46.0, envy-feasible: True
#7: 4, stable: 41, max: 45.0, envy-feasible: True
#8: 3, stable: 41, max: 43.0, envy-feasible: True
#9: 2, stable: 43, max: 45.0, envy-feasible: True
#10: 4, stable: 43, max: 47.0, envy-feasible: False
master
n1_1000_n2_1000_k_5
#1: 97, stable: 782, max: 876.0, envy-feasible: False
#2: 93, stable: 768, max: 861.0, envy-feasible: False
#3: 97, stable: 752, max: 849.0, envy-feasible: True
#4: 109, stable: 738, max: 847.0, envy-feasible: False
#5: 106, stable: 754, max: 860.0, envy-feasible: False
#6: 91, stable: 757, max: 847.0, envy-feasible: False
#7: 89, stable: 761, max: 850.0, envy-feasible: True
#8: 105, stable: 770, max: 872.0, envy-feasible: False
#9: 95, stable: 750, max: 845.0, envy-feasible: False
#10: 91, stable: 762, max: 852.0, envy-feasible: False
n1_1000_n2_1000_k_10
#1: 112, stable: 837, max: 949.0, envy-feasible: False
#2: 111, stable: 845, max: 956.0, envy-feasible: True
#3: 94, stable: 846, max: 940.0, envy-feasible: False
#4: 105, stable: 827, max: 932.0, envy-feasible: False
#5: 106, stable: 826, max: 932.0, envy-feasible: False
#6: 119, stable: 821, max: 940.0, envy-feasible: False
#7: 91, stable: 854, max: 945.0, envy-feasible: True
#8: 101, stable: 830, max: 931.0, envy-feasible: False
#9: 101, stable: 833, max: 933.0, envy-feasible: False
#10: 118, stable: 831, max: 949.0, envy-feasible: False
n1_1000_n2_1000_k_20
#1: 97, stable: 891, max: 988.0, envy-feasible: True
#2: 87, stable: 899, max: 986.0, envy-feasible: True
#3: 98, stable: 884, max: 982.0, envy-feasible: False
#4: 109, stable: 879, max: 988.0, envy-feasible: False
#5: 91, stable: 899, max: 990.0, envy-feasible: False
#6: 95, stable: 884, max: 979.0, envy-feasible: False
#7: 92, stable: 899, max: 991.0, envy-feasible: False
#8: 98, stable: 881, max: 979.0, envy-feasible: False
#9: 95, stable: 888, max: 983.0, envy-feasible: False
#10: 84, stable: 909, max: 993.0, envy-feasible: True
n1_500_n2_500_k_5
#1: 47, stable: 380, max: 427.0, envy-feasible: True
#2: 58, stable: 376, max: 434.0, envy-feasible: False
#3: 50, stable: 377, max: 426.0, envy-feasible: False
#4: 49, stable: 382, max: 430.0, envy-feasible: True
#5: 52, stable: 385, max: 437.0, envy-feasible: False
#6: 43, stable: 378, max: 421.0, envy-feasible: True
#7: 56, stable: 373, max: 428.0, envy-feasible: False
#8: 43, stable: 369, max: 412.0, envy-feasible: False
#9: 63, stable: 369, max: 431.0, envy-feasible: True
#10: 52, stable: 376, max: 426.0, envy-feasible: False
n1_500_n2_500_k_10
#1: 52, stable: 421, max: 473.0, envy-feasible: False
#2: 63, stable: 406, max: 469.0, envy-feasible: False
#3: 53, stable: 416, max: 469.0, envy-feasible: False
#4: 56, stable: 418, max: 474.0, envy-feasible: False
#5: 51, stable: 415, max: 466.0, envy-feasible: True
#6: 50, stable: 424, max: 474.0, envy-feasible: True
#7: 63, stable: 411, max: 474.0, envy-feasible: False
#8: 63, stable: 414, max: 476.0, envy-feasible: False
#9: 42, stable: 420, max: 462.0, envy-feasible: False
#10: 48, stable: 426, max: 474.0, envy-feasible: False
n1_500_n2_500_k_20
#1: 41, stable: 452, max: 493.0, envy-feasible: True
#2: 45, stable: 444, max: 489.0, envy-feasible: True
#3: 50, stable: 447, max: 497.0, envy-feasible: False
#4: 41, stable: 453, max: 494.0, envy-feasible: True
#5: 44, stable: 452, max: 496.0, envy-feasible: True
#6: 49, stable: 449, max: 498.0, envy-feasible: False
#7: 41, stable: 451, max: 492.0, envy-feasible: True
#8: 48, stable: 447, max: 495.0, envy-feasible: True
#9: 47, stable: 445, max: 492.0, envy-feasible: False
#10: 47, stable: 439, max: 486.0, envy-feasible: True
n1_100_n2_100_k_5
#1: 13, stable: 70, max: 82.0, envy-feasible: True
#2: 11, stable: 77, max: 88.0, envy-feasible: False
#3: 10, stable: 77, max: 87.0, envy-feasible: True
#4: 11, stable: 77, max: 87.0, envy-feasible: True
#5: 11, stable: 75, max: 86.0, envy-feasible: True
#6: 9, stable: 80, max: 89.0, envy-feasible: True
#7: 14, stable: 78, max: 92.0, envy-feasible: True
#8: 13, stable: 75, max: 88.0, envy-feasible: True
#9: 11, stable: 73, max: 83.0, envy-feasible: True
#10: 8, stable: 77, max: 85.0, envy-feasible: True
n1_100_n2_100_k_10
#1: 12, stable: 81, max: 93.0, envy-feasible: True
#2: 10, stable: 87, max: 97.0, envy-feasible: True
#3: 11, stable: 84, max: 95.0, envy-feasible: True
#4: 14, stable: 82, max: 96.0, envy-feasible: False
#5: 10, stable: 84, max: 94.0, envy-feasible: True
#6: 12, stable: 84, max: 96.0, envy-feasible: True
#7: 8, stable: 86, max: 94.0, envy-feasible: True
#8: 11, stable: 84, max: 95.0, envy-feasible: True
#9: 6, stable: 87, max: 93.0, envy-feasible: True
#10: 10, stable: 82, max: 92.0, envy-feasible: True
n1_100_n2_100_k_20
#1: 14, stable: 84, max: 98.0, envy-feasible: True
#2: 11, stable: 88, max: 99.0, envy-feasible: True
#3: 9, stable: 90, max: 99.0, envy-feasible: True
#4: 8, stable: 91, max: 99.0, envy-feasible: True
#5: 13, stable: 86, max: 99.0, envy-feasible: True
#6: 9, stable: 89, max: 98.0, envy-feasible: True
#7: 11, stable: 89, max: 100.0, envy-feasible: True
#8: 12, stable: 87, max: 99.0, envy-feasible: True
#9: 9, stable: 90, max: 99.0, envy-feasible: True
#10: 10, stable: 89, max: 99.0, envy-feasible: True
n1_50_n2_50_k_5
#1: 7, stable: 36, max: 43.0, envy-feasible: True
#2: 8, stable: 36, max: 44.0, envy-feasible: True
#3: 3, stable: 38, max: 41.0, envy-feasible: True
#4: 4, stable: 37, max: 41.0, envy-feasible: True
#5: 10, stable: 39, max: 49.0, envy-feasible: False
#6: 5, stable: 41, max: 46.0, envy-feasible: True
#7: 6, stable: 37, max: 43.0, envy-feasible: True
#8: 3, stable: 41, max: 44.0, envy-feasible: True
#9: 4, stable: 39, max: 43.0, envy-feasible: True
#10: 6, stable: 38, max: 44.0, envy-feasible: True
